package test.arbn.opektestproject

import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class MainActivityUI : AnkoComponent<MainActivity> {
    private var isVisible: Boolean = false
    private var isRight: Boolean = true
    private var isTop: Boolean = true
    private var numberOfShapes: Int = 0
    override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
        frameLayout {
            frameLayout {
                backgroundResource = R.drawable.view_boarder
            }.lparams(width = matchParent, height = dip(200)) {
                foregroundGravity = Gravity.CENTER
                margin = dip(20)
            }
            val hostFrameLayoutOne = relativeLayout {
                imageView {
                    backgroundResource = R.drawable.shape_one
                }.lparams(width = dip(100), height = dip(100)) {
                    margin = dip(20)
                    alignParentEnd()
                    visibility = View.INVISIBLE
                }
            }.lparams(width = matchParent, height = dip(240))
            val hostFrameLayoutTwo = relativeLayout {
                imageView {
                    backgroundResource = R.drawable.shape_two
                }.lparams(width = dip(100), height = dip(100)) {
                    margin = dip(20)
                    alignParentStart()
                    visibility = View.INVISIBLE
                }
            }.lparams(width = matchParent, height = dip(240))
            linearLayout {
                button("Hor Align") {
                    onClick {
                        if (!isRight) {
                            hostFrameLayoutOne.gravity = Gravity.RIGHT
                            hostFrameLayoutTwo.gravity = Gravity.LEFT
                            isRight = true
                        } else {
                            hostFrameLayoutOne.gravity = Gravity.LEFT
                            hostFrameLayoutTwo.gravity = Gravity.RIGHT
                            isRight = false
                        }
                    }
                }
                button("Ver Align") {
                    onClick {
                        if (!isTop) {
                            hostFrameLayoutOne.gravity = Gravity.TOP
                            hostFrameLayoutTwo.gravity = Gravity.BOTTOM
                            isTop = true
                        } else {
                            hostFrameLayoutOne.gravity = Gravity.BOTTOM
                            hostFrameLayoutTwo.gravity = Gravity.TOP
                            isTop = false
                        }
                    }
                }
            }.lparams {
                gravity = Gravity.CENTER
                topMargin = dip(100)
            }
            linearLayout {
                button("Create or Delete Shape") {
                    onClick {
                        if (!isVisible) {
                            if (numberOfShapes == 0) {
                                hostFrameLayoutOne.visibility = View.VISIBLE
                                numberOfShapes++
                            } else if (numberOfShapes == 1) {
                                hostFrameLayoutTwo.visibility = View.VISIBLE
                                numberOfShapes++
                            }
                            if (numberOfShapes == 2) {
                                isVisible = true
                            }
                        } else {
                            if (numberOfShapes == 2) {
                                hostFrameLayoutTwo.visibility = View.INVISIBLE
                                numberOfShapes--
                            } else if (numberOfShapes == 1) {
                                hostFrameLayoutOne.visibility = View.INVISIBLE
                                numberOfShapes--
                            }
                            if (numberOfShapes == 0) {
                                isVisible = false
                            }
                        }
                    }
                }
            }.lparams {
                gravity = Gravity.CENTER
                topMargin = dip(50)
            }
            linearLayout {
                orientation = LinearLayout.VERTICAL
                textView("Developed by: AhmadReza BahmanNejad")
                textView("Email: Mr_ARBN@yahoo.com")
                textView("Phone Number: +98912-495-4426")
            }.lparams {
                gravity = Gravity.BOTTOM
                margin = dip(20)
            }
        }
    }
}